#ifndef ME_INPUT
#define ME_INPUT

struct KeyCodes {
enum Keys {
    UNKNOWN,
    BACKSPACE,
    TAB,
    RETURN,
    ESCAPE,
    SPACE,
    EXCLAMATION_MARK,
    DOUBLE_QUOTATION_MARKS,
    HASH,
    DOLLAR,
    PERCENT,
    AMPERSAND,
    QUOTATION_MARKS,
    LEFT_PARENTHESIS,
    RIGHT_PARENTHESIS,
    ASTERISK,
    PLUS,
    COMMA,
    MINUS,
    PERIOD,
    SLASH,
    NUM_0,
    NUM_1,
    NUM_2,
    NUM_3,
    NUM_4,
    NUM_5,
    NUM_6,
    NUM_7,
    NUM_8,
    NUM_9,
    COLON,
    SEMICOLON,
    LESS_THAN,
    EQUALS,
    GREATER_THAN,
    QUESTION_MARK,
    AT,
    LEFT_BRACKET,
    BACKSLASH,
    RIGHT_BRACKET,
    CARET,
    UNDERSCORE,
    BACKQUOTE,
    A,
    B,
    C,
    D,
    E,
    F,
    G,
    H,
    I,
    J,
    K,
    L,
    M,
    N,
    O,
    P,
    Q,
    R,
    S,
    T,
    U,
    V,
    W,
    X,
    Y,
    Z,
    DEL,
    CAPSLOCK,
    F1,
    F2,
    F3,
    F4,
    F5,
    F6,
    F7,
    F8,
    F9,
    F10,
    F11,
    F12,
    PRINT_SCREEN,
    SCROLL_LOCK,
    PAUSE,
    INSERT,
    HOME,
    PAGEUP,
    END,
    PAGEDOWN,
    RIGHT,
    LEFT,
    DOWN,
    UP,
    NUM_LOCK,
    LCTRL,
    LSHIFT,
    LALT,
    LGUI,
    RCTRL,
    RSHIFT,
    RALT,
    RGUI,
    LEFT_MOUSE_BUTTON,
    RIGHT_MOUSE_BUTTON,
    MIDDLE_MOUSE_BUTTON
};
};

struct me_input {
    bool keys[108];
	
    bool quit;
	int mouseRelX;
	int mouseRelY;
	int mouseX;
	int mouseY;
    
    int screenWidth;
    int screenHeight;
    
    bool isDown(int keyCodeEnum);
    bool isUp(int keyCodeEnum);
};

me_input setupInput(int screen_width, int screen_height);
void processInput(me_input &input);


#endif
