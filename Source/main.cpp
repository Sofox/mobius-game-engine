/*
 * main.cpp
 * Mobius Engine Project
 * Author: Sofox
 * Date: 24th November 2012
 * Summary: The main source file of the Mobius Game engine, currently a stub.
 *
*/
#include "SDL/SDL.h"
#include "SDL/SDL_opengl.h"
#include <GL/glu.h>

#include <iostream>
#include <sstream>
#include <math.h>
#include "math.h"
#include "input.h"

//fps vars - sofox, is there a better place to put these?
#define FPS_INTERVAL 1.0 //seconds.

//temporary, can be removed later

struct me_rotation{
    double x,y,z;
};
struct me_position{
	double x,y,z;
};
struct me_camera{
    me_position pos;
    struct me_rotation rot;
    double trigSin,trigCos;
    bool controlModeMouse;
};
struct me_item{
    double x,y,z, height, radius;
};
struct me_collision_sphere{
    me_position pos;
    double radius;
};
struct me_character{
    me_position pos;
    struct me_rotation rot;
    struct me_collision_sphere col;
    double radius;
    double height;
};
struct me_level{

};

struct me_triangle{
    me_position p1;
    me_position p2;
    me_position p3;
};
struct me_collision_mesh{
    me_triangle triangles[20];
    int triangleCount;

	me_collision_mesh() {
		triangleCount = 0;
		memset(&triangles, 0, 20);
	}
};

struct me_world{
    me_character character;
    me_camera camera;
    me_collision_mesh colMesh;
};

void renderCharacter(me_character &character){
    glPushMatrix();
    glTranslatef(character.pos.x, character.pos.y, character.pos.z);
    glRotatef(character.rot.y,0.0,1.0,0.0);
    glBegin(GL_QUADS);
        glColor4f(0.2,0.2,0.8,1.0);        
        glVertex3f(-character.radius, 0, 0);
        glVertex3f( character.radius, 0, 0);
        glVertex3f( character.radius, character.height, 0);
        glVertex3f(-character.radius, character.height, 0);
    glEnd();
    glPopMatrix();
}

void drawTriangle(me_triangle &triangle){
    glBegin(GL_TRIANGLES);
        glVertex3f(triangle.p1.x, triangle.p1.y, triangle.p1.z);
        glVertex3f(triangle.p2.x, triangle.p2.y, triangle.p2.z);
        glVertex3f(triangle.p3.x, triangle.p3.y, triangle.p3.z);
    glEnd();


}

void render(me_world &world, float deltaTime){
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
    
    //Camera control
    glRotatef(-world.camera.rot.y,0.0,1.0,0.0);
    //Some more trig for the local X axis - needs optimizing
    glRotatef(world.camera.rot.x,world.camera.trigCos,0.0,-world.camera.trigSin);
    glTranslatef(-world.camera.pos.x,-world.camera.pos.y,-world.camera.pos.z);
    
    //Begin rendering
    glBegin(GL_QUADS);
        glColor4f(0.1,0.8,0.0,1.0);
        glVertex3f(-100,0,-100);
        glVertex3f(100,0,-100);
        glVertex3f(100,0,100);
        glVertex3f(-100,0,100);
    glEnd();
    
    renderCharacter(world.character);

    glBegin(GL_QUADS);
        glColor4f(1.0,1.0,1.0,1.0);
        glVertex3f(1,0,-12);
        glVertex3f(3,0,-12);
        glVertex3f(3,2,-12);
        glVertex3f(1,2,-12);
    glEnd();

    glColor4f(1.0,0.9,0.5,0.9);
    
    for(int i = 0; i< world.colMesh.triangleCount; i++){
        drawTriangle(world.colMesh.triangles[i]);
    }

    /*glBegin(GL_TRIANGLES);
        glColor4f(1.0,0.9,0.5,0.9);
        glVertex3f(-20,0,12);
        glVertex3f(-20,0,14);
        glVertex3f(-20,10,13);
    glEnd();

    glBegin(GL_TRIANGLES);
        glColor4f(0.2,0.9,0.57,0.9);
        glVertex3f(22,0,-45);
        glVertex3f(24,0,-45);
        glVertex3f(23,9,-45);
    glEnd();*/

    glLoadIdentity();
        
    SDL_GL_SwapBuffers();
}

bool init_GL(int screen_width, int screen_height){    
    glClearColor( 0, 0, 0, 0 );

    glMatrixMode( GL_PROJECTION );
    glLoadIdentity();
    glViewport(0, 0, screen_width, screen_height);      

    gluPerspective(45.0f,(GLfloat)screen_width/(GLfloat)screen_height,0.1f,100.0f);
 
    glMatrixMode(GL_MODELVIEW);
    glEnable(GL_DEPTH_TEST);
    glLoadIdentity();

    //If there was any errors    
    if( glGetError() != GL_NO_ERROR ) {
        std::cerr << "Unable to start OpenGL\n";
        return false;
    }
    return true;
}

bool collisionSphereScenery(me_position &pos, float radius){

    return false;
}

me_position moveCharacter(me_character &character, double moveX, double moveY, double moveZ){
    me_position pos;
    me_position colPos;

    pos.x = (character.pos.x + moveX);    colPos.x = (character.pos.x + moveX);
    pos.y = (character.pos.y + moveY);    colPos.y = (character.pos.y + moveY);
    pos.z = (character.pos.z + moveZ);    colPos.z = (character.pos.z + moveZ);

    if(collisionSphereScenery(colPos, character.col.radius))
        return character.pos;
    else
        return pos;
}

float sensitivity = 1.0f;
bool sensitivityChange = true;

void update(me_input &input, me_world &world, float deltaTime){
    if(input.isDown(KeyCodes::LEFT))
        world.character.rot.y += 90 * deltaTime;
    if(input.isDown(KeyCodes::RIGHT))
        world.character.rot.y -= 90 * deltaTime;
            
    if(input.isDown(KeyCodes::S) && !world.camera.controlModeMouse){
        world.camera.controlModeMouse = true;
        input.keys[KeyCodes::S] = false; }
    else if(input.isDown(KeyCodes::S)){
        world.camera.controlModeMouse = false;
        input.keys[KeyCodes::S] = false; }

    //define our working vars
    double angleY = world.character.rot.y*DEG2RAD;
    double sinAngleY = sin(angleY);
    double cosAngleY = cos(angleY);

    if(input.isDown(KeyCodes::UP) || input.isDown(KeyCodes::DOWN)){
        double move = input.isDown(KeyCodes::UP) ? (-15 * deltaTime):(15 * deltaTime);
        world.character.pos = moveCharacter(world.character, sinAngleY*move, 0, cosAngleY*move);
    }
    //Compute where the camera is in relation to character
    double cameraDist = 6;
    
    //Switch our working vars to camera
    angleY = world.camera.rot.y*DEG2RAD;
    sinAngleY = sin(angleY);
    cosAngleY = cos(angleY);
    double angleX = world.camera.rot.x*DEG2RAD;
    double cosX = cos(angleX);
    world.camera.trigSin = sinAngleY * cosX;
    world.camera.trigCos = cosAngleY * cosX;
    
    if(input.isDown(KeyCodes::O)) {
        if(sensitivityChange) {
            sensitivity -= 0.1f;
            sensitivityChange = false;
        }
    }
    else if(input.isDown(KeyCodes::P)) {
        if(sensitivityChange) {
            sensitivity += 0.1f;
            sensitivityChange = false;
        }
    }
    else {
        sensitivityChange = true; }
        
    sensitivity = clamp(sensitivity, 0, 2);
    
    //Make new position with trig
    world.camera.pos.x = (world.character.pos.x+(world.camera.trigSin*cameraDist));
    world.camera.pos.y = (world.character.pos.y + 1 + (sin(angleX)*cameraDist));
    world.camera.pos.z = (world.character.pos.z+(world.camera.trigCos*cameraDist));
    world.camera.rot.x += input.mouseRelY * sensitivity * 0.2;
    //No going higher than 90!
    if(world.camera.rot.x > 90)
        world.camera.rot.x = 90;
    if(world.camera.rot.x < -90)
        world.camera.rot.x = -90;
    //rotate if mode is set
    if(world.camera.controlModeMouse) {
        world.camera.rot.y += input.mouseRelX * -sensitivity * 0.2; }
    else {
        if(world.camera.rot.y != world.character.rot.y) {
            world.camera.rot.y = lerp(world.camera.rot.y, world.character.rot.y, 4*deltaTime);
        }
    }
    input.mouseRelX = 0;
    input.mouseRelY = 0;
}

me_camera setupCamera(){
    struct me_camera camera;
    camera.pos.x = (0);
    camera.pos.y = (1);
    camera.pos.z = (6);
	camera.rot.x = 0;
    camera.rot.y = 0;
    camera.trigSin = 0;
    camera.trigCos = 0;
    camera.controlModeMouse = false;
    return camera;
}

me_collision_mesh addTriangle(me_collision_mesh &mesh,
        double x1, double y1, double z1,
        double x2, double y2, double z2,
        double x3, double y3, double z3){
    me_triangle t;
    t.p1.x = (x1); t.p1.y = (y1); t.p1.z = (z1);
    t.p2.x = (x2); t.p2.y = (y2); t.p2.z = (z2);
    t.p3.x = (x3); t.p3.y = (y3); t.p3.z = (z3);
    mesh.triangles[mesh.triangleCount] = t;
    mesh.triangleCount++;
    return mesh;
}
me_world setupWorld(){
    me_world world;
    world.character.pos.x = (0);
    world.character.pos.y = (0);
    world.character.pos.z = (0);
    world.character.rot.y=0;
    world.character.col.pos.x = (0);
    world.character.col.pos.y = (0.25);
    world.character.col.pos.z = (0);
    world.character.col.radius = 0.25;
    world.character.radius = 0.25;
    world.character.height = 1;
    
    world.camera = setupCamera(); 

	world.colMesh = me_collision_mesh();

    world.colMesh = addTriangle(world.colMesh,-20, 0, 12,-20,0, 14,-20,10,13);
    world.colMesh = addTriangle(world.colMesh, 22, 0,-45, 24,0,-45, 23,9, -45);
        
    return world;
}

int main(int argc, char** argv)
{
    int screen_width = 640;
    int screen_height = 480;
    bool resizable = false;
    bool fullscreen = false;

    std::cout << "Starting up...\n";
    if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER) < 0) {
        std::cerr << "Unable to start SDL\n";
        return -1;
    }
    
    int fps_lasttime = SDL_GetTicks(); //the last recorded time.
    int fps_current = 0; //the current FPS.
    int fps_frames = 0; //frames passed since the last recorded fps.

    if ( SDL_SetVideoMode(screen_width, screen_height,0,SDL_OPENGL | (resizable ? SDL_RESIZABLE : 0) | (fullscreen ? SDL_FULLSCREEN : 0)) == NULL) {
        std::cerr << "Unable to set Video Mode\n";
        return -1;
    }

    if( init_GL(screen_width, screen_height) == false ) {    
    return false;
    }

    SDL_WM_SetCaption("Mobius Engine", NULL);
	SDL_ShowCursor(0);
    SDL_WM_GrabInput( SDL_GRAB_ON );

    struct me_input input = setupInput(screen_width, screen_height);
    struct me_world world = setupWorld();
    //prepare delta/time variables
    float deltaTime = 0.0;
    int lastTime = 0;
    int thisTime = 0;
    
    std::cout << "Starting game loop...\n";
    bool quit = false;
    while(!quit){
        
        thisTime = SDL_GetTicks();
        deltaTime = (float)(thisTime - lastTime) / 1000;
        lastTime = thisTime;
        
        //we need to know fps. can be removed later if we want
        fps_frames++;
        if (fps_lasttime < SDL_GetTicks() - FPS_INTERVAL*1000)
        {
            fps_lasttime = SDL_GetTicks();
            fps_current = fps_frames;
            fps_frames = 0;
      
            std::ostringstream oss;
            oss << "Mobius Engine - FPS: " << fps_current << "; Mouse Sensitivity: " << sensitivity;
            SDL_WM_SetCaption(oss.str().c_str(), NULL);
        }
        
        processInput(input);
        if(input.quit || input.isDown(KeyCodes::ESCAPE))
            quit = true;
        update(input,world,deltaTime);
        render(world,deltaTime);
        SDL_Delay(1);
    }
    SDL_ShowCursor(1);
    SDL_Quit();
    return 0;
}
